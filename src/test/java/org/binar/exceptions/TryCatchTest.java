package org.binar.exceptions;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TryCatchTest {

  TryCatch tryCatch;

  int hasilJumlah;
  int hasilKurang;

  @BeforeAll
  static void beforeAll() {
    int hasilJumlah = 0;
  }
  @BeforeEach
  void init() {
    tryCatch = new TryCatch();
  }

  @AfterEach
  void afterEach() {
    hasilJumlah = 0;
  }

  @Test
  @DisplayName("Penjumlahan dua angka")
  void penjumlahan_duaAngka() {
    hasilJumlah = tryCatch.penjumlahan("1 10");
    Assertions.assertEquals(11, hasilJumlah);
  }

  @Test
  void penjumlahan_null() {
    hasilJumlah = tryCatch.penjumlahan(null);
    Assertions.assertEquals(0, hasilJumlah);
//    Assertions.assertThrows(NullPointerException.class, () -> tryCatch.penjumlahan(null));
  }

  @Test
  void penjumlahan_satuAngkaSatuHuruf() {
    Assertions.assertEquals(0, tryCatch.penjumlahan("1 l"));
  }

  @Test
  void pengurangan_duaAngka() {
    hasilKurang = tryCatch.pengurangan("10 5");
    Assertions.assertEquals(5, hasilKurang);
  }

  @Test
  void pengurangan_null() {
    hasilKurang = tryCatch.pengurangan(null);
    Assertions.assertEquals(0, hasilKurang);
  }

  @Test
  void pnegurangan_satuAngkaSatuHuruf() {
    hasilKurang = tryCatch.pengurangan("10 l");
    Assertions.assertEquals(0, hasilKurang);
  }

}
