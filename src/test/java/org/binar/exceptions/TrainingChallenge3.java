package org.binar.exceptions;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.Vector;
import org.binar.challenge3.FileHandling;
import org.binar.challenge3.FileHandlingImpl;
import org.binar.challenge3.Menu;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class TrainingChallenge3 {

  // inisialisasi mock cara pertama
  @Spy
  List<String> dataNilai = new ArrayList<>();

  @Mock
  File file = new File("/mock/path");

//  @Spy
  Scanner scan = new Scanner(System.in);

  List<String> dataNilai2;

  FileHandling fileHandling;

  Menu menu;

  @BeforeEach
  void init() {
    // inisialisasi mock cara kedua
    dataNilai2 = Mockito.mock(ArrayList.class);
    fileHandling = new FileHandlingImpl(dataNilai, file);
    menu = new Menu(scan);
  }

  @Test
  void readFile_Success() {
    String path = "/Users/hf-user/Desktop/work/Binar/Chapter-2/src/main/resources/dataDiri.csv";
//    Mockito.when(file.exists()).thenReturn(true);
    List<String> resultList = Assertions.assertDoesNotThrow(()->fileHandling.readFile(path));
//    Assertions.assertDoesNotThrow(() -> fileHandling.readFile(path));
    Mockito.verify(dataNilai, Mockito.times(4)).add(Mockito.anyString());
    Assertions.assertEquals(4, resultList.size());
  }

  @Test
  void readFile_Failed_FileNotFound() {
    String path = "/TotallyWrongPath";
    Assertions.assertThrows(IOException.class, () -> fileHandling.readFile(path));
  }

  @Test
  void menu_ExitInput() {
//    Mockito.when(scan.nextInt()).thenReturn(1);
    String input = "0";
    InputStream in = new ByteArrayInputStream(input.getBytes());
    System.setIn(in);
    menu.mainMenu(new Scanner(input));
  }

  @Test
  void menu_NullInput() {
    Assertions.assertThrows(InputMismatchException.class, () -> menu.mainMenu(new Scanner("null")));
  }


}
