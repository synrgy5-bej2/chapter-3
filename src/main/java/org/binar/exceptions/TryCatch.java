package org.binar.exceptions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TryCatch {

  public Integer pengurangan(String data) {
    try{
      String[] dataArr = data.split(" ");
      int total = Integer.valueOf(dataArr[0]) + Integer.valueOf(dataArr[1]);
      return total;
    } catch(NullPointerException|NumberFormatException npe) {

    }
    return 0;
  }

  // 1 4 = 5
  public Integer penjumlahan(String data) throws NullPointerException {

    try{
      String[] dataArr = data.split(" ");
      int total = Integer.valueOf(dataArr[0]) + Integer.valueOf(dataArr[1]);
      System.out.println("total nya adalah : " + total);
      return total;
    } catch(NumberFormatException|NullPointerException nfe) {
      System.out.println("Error in Class " + this.getClass() + " method " + new Exception()
          .getStackTrace()[0].getMethodName());
      return 0;
    } catch(IndexOutOfBoundsException oob) {
      System.out.println("jumlahnya kurang jadi saya nolkan : 0");
      return 0;
    }
    // masukkan business logic
//    System.out.println("business logic nih");
  }

  public void tryCatchFinally(String data) {
    try {

    } catch(Exception e) {

    } finally {
      System.out.println("masuk blok finally gengs");
    }
  }

  public void tryCatchWithResource() {
    try(Scanner scan = new Scanner(System.in)) {
      String data = scan.nextLine();
    } catch(Exception e) {

    } finally {
      System.out.println("finally block try catch with resources");
    }
  }

  public void throwsExample(String data) throws IOException, NullPointerException, ArithmeticException {
    File file = new File(""); // ngambil detail file yg kita masukan lokasi file nya
    FileReader reader = new FileReader(file);
    BufferedReader br = new BufferedReader(reader);
    String line = "";
    String[] tempArr;
    List<Integer> listDd = new ArrayList<>();
    while((line = br.readLine()) != null) {
      tempArr = line.split(",");

    }
    String line1 = "1234,Rizky,2010-15-15,Sunnyvale CA,0817276x";
    String[] line1Arr = {"1234", "Rizky", "2010-15-15", "Sunnivale CA", "0817276x"};
    if(data == null) {
      throw new IOException("null nih beb");
    }
    br.close();
  }

  public void throwNewExample(String data) {
    try{
      Integer num = Integer.valueOf(data);
      if(num % 2 == 0) {
        throw new RizkyExceptions("Didapat angka yang genap", new Exception());
      }
    } catch(Exception e) {
      e.printStackTrace();
    }
  }

}
