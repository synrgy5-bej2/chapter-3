package org.binar.exceptions;

public class RizkyExceptions extends RuntimeException {

  public RizkyExceptions(String errorMessage) {
    super(errorMessage);
  }

  public RizkyExceptions(String errorMessage, Throwable err) {
    super(errorMessage, err);
  }

}
