package org.binar.model;

import java.util.Optional;
import lombok.Data;

@Data
public class Computer {

  private SoundCard soundCard;
  private Optional<SoundCard> soundCardOpt;

}
