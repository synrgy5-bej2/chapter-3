package org.binar.model;

import java.util.Optional;
import lombok.Data;

@Data
public class SoundCard {

  private USB usb;
  private Optional<USB> usbOpt;

}
