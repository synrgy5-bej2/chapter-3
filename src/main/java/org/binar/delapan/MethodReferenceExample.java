package org.binar.delapan;

public class MethodReferenceExample {

  public MethodReferenceExample(){

  }

  public MethodReferenceExample(String param) {
    System.out.println(param + "constructor");
  }

  public static void methodRefStatic(String param) {
    System.out.println(param + "static");
  }

  public void methodRefInstance(String param) {
    System.out.println(param + "instance");
  }

}
