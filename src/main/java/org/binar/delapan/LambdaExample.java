package org.binar.delapan;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class LambdaExample {

  Consumer<String> testConsumer = (text) -> {
    System.out.println(text);
  };

  public void functional() {
    Consumer<String> consumer = (text) -> System.out.println(text);
    BiConsumer<String, Integer> biConsumer = (text, num) -> System.out.println(text + num);
    Supplier<String> supplier = () -> {
      int a = 10;
      int b = 15;
      return String.valueOf(a * b * 10/100);
    };

    consumer.accept("Kholis ganteng"); // consumer("Kholis ganteng");
    biConsumer.accept("Umurku", 17); // biConsumer("Umurku", 17);
    String hasilSup = supplier.get(); // methodSupplier();
    System.out.println(hasilSup);
  }

  public void consumer(String text) {
    System.out.println(text);
  }

  public void biConsumer(String text, Integer num) {
    System.out.println(text + num);
  }

  public String methodSupplier() {
    return "ini tambah supplier";
  }

}
