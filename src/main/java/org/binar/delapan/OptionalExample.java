package org.binar.delapan;

import java.util.Optional;
import org.binar.exceptions.TryCatch;
import org.binar.model.Computer;
import org.binar.model.SoundCard;
import org.binar.model.USB;

public class OptionalExample {

  public void withoutOptional() {
    TryCatch tryCatch = null;
    // logic sehingga variable trycatch terisi

    // karena tanpa optional, null harus di cek
    if(tryCatch != null) {

    } else {

    }
  }

  public void withOptional() {
    Optional<TryCatch> tryCatch1 = Optional.empty();
    TryCatch tryCatchObj = new TryCatch();
    TryCatch tryCatchObjNul = null;
    Optional<TryCatch> tryCatch2 = Optional.of(tryCatchObj);
    Optional<TryCatch> tryCatch3 = Optional.of(new TryCatch());

    Optional<TryCatch> tryCatch4 = Optional.ofNullable(new TryCatch());

    Optional<TryCatch> tryCatch5 = Optional.of(tryCatchObjNul);
    Optional<TryCatch> tryCatch6 = Optional.ofNullable(tryCatchObjNul);

    if(tryCatch1.isPresent()) {

    }
  }

  public void moduleExampleWOOptional() {
    Computer computer = new Computer();
    SoundCard soundCard = new SoundCard();
    USB usb = new USB();
//    usb.setVersion("1.0.0");
    soundCard.setUsb(usb);
    computer.setSoundCard(soundCard);

    String version = "";
    if(computer != null) {
      if(computer.getSoundCard() != null) {
        SoundCard sc = computer.getSoundCard();
        if(sc.getUsb() != null) {
          USB u = sc.getUsb();
          if(u.getVersion() != null) {
            version = computer.getSoundCard().getUsb().getVersion();
            System.out.println(version);
          } else {
            version = "UNKNOWN";
          }
        } else {
          version = "UNKNOWN";
        }
      } else {
        version = "UNKNOWN";
      }
    } else {
      version = "UNKNOWN";
    }
//    String version = computer.getSoundCard().getUsb().getVersion();

//    System.out.println(version.split("."));
  }

  public void moduleExampleWithOptional() {
    Optional<Computer> computer = Optional.of(new Computer());
    Optional<SoundCard> soundCard = Optional.of(new SoundCard());
    Optional<USB> usb = Optional.of(new USB());
    usb.get().setVersion("1.0.0");
    soundCard.get().setUsbOpt(usb);
    computer.get().setSoundCardOpt(soundCard);


    String version = computer
        .flatMap((comp) -> comp.getSoundCardOpt())
        .flatMap(SoundCard::getUsbOpt)
        .map(USB::getVersion).orElse("UNKNOWN");

    System.out.println(version);
  }

}
