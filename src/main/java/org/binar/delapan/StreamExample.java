package org.binar.delapan;

import java.util.*;
import java.util.stream.*;
import org.binar.model.USB;

/**
 *
 */
public class StreamExample {

  public void makeStream() {
    Stream<String> stream1 = Stream.of("Rizky", "Mochamad", "Fauzi");
    Stream<String> stream2 = Stream.empty();
    String data = null;
    Stream<String> stream3 = Stream.of(data, data, data);
    List<String> stringList = new ArrayList<>();
    Stream<String> stream4 = stringList.stream();
    Set<String> stringSet = new HashSet<>();
    Stream<String> stream5 = stringSet.stream();
  }

  public void operation() {
    List<String> stringList = Arrays.asList("Meja", "Kursi", "Kasur", "Sofa", "Lemari");

    // Proses data dengan loop biasa
    for(int i = 0; i < stringList.size(); i++) {
      // tambah kata "Ada "
      // proses filter
      // kumpulin data jadi 1 variable
    }

    // Proses data dengan forEach loop
    for(String furniture: stringList) {
      // tambah kata "Ada "
      // proses filter
      // kumpulin data jadi 1 variable
    }

    // Proses data list dengan stream
    List<String> processedList = stringList.stream() // Pembuatan sumber data nya atau sumber aliran nya
        .map(furniture -> "Ada " + furniture) // Proses masing-masing data ditambah kata "Ada " : "Ada Meja", "Ada Kursi" ...
        .filter(furniture -> furniture.contains("K")) // Proses filter untuk mengambil data yang hanya mengandung huruf "K"
        .collect(Collectors.toList()); // Mengumpulkan data menjadi 1 variable berbentuk List.

    processedList.forEach(System.out::println);
  }

  public void operationObj() {
    List<Integer> kelasA = Arrays.asList(10, 9, 6, 8);
    List<Integer> kelasB = Arrays.asList(6, 9, 10, 9);
    List<List<Integer>> nilaiSekolah = Arrays.asList(kelasA, kelasB);

    List<Integer> nilaiSatuSekolah = nilaiSekolah.stream()
        .flatMap(nilai -> nilai.stream())
        .sorted(Comparator.reverseOrder())
        .collect(Collectors.toList());
    nilaiSatuSekolah.forEach(System.out::println);

    double mean = nilaiSatuSekolah.stream().reduce(0, Integer::sum) / (int) nilaiSatuSekolah.stream().count();
    double meanDisediakan = nilaiSatuSekolah.stream().collect(Collectors.averagingInt(Integer::intValue));
    System.out.println("mean ngide : " + mean);
    System.out.println("mean dah ada : " + meanDisediakan);

    int max = nilaiSatuSekolah.stream().max(Comparator.comparing(Integer::intValue)).get();
    int min = nilaiSatuSekolah.stream().min(Comparator.comparing(Integer::intValue)).get();
    System.out.println("nilai max : " + max);
    System.out.println("nilai min : " + min);
  }

  public void processUSB() {
    USB usb1 = new USB("1.0.0", "Kingston", 100000);
    USB usb2 = new USB("1.5.6", "V GEn", 60000);
    USB usb3 = new USB("3.0.0", "Intel", 1000000);
    List<USB> usbList = Arrays.asList(usb1, usb2, usb3);
    usbList.stream()
        .sorted(Comparator.comparingInt(USB::getPrice))
        .forEach(usb -> System.out.println(usb.getBrand()));

  }

}
