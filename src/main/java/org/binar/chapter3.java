package org.binar;

import java.io.IOException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import javax.swing.text.html.Option;
import org.binar.delapan.LambdaExample;
import org.binar.delapan.MethodReferenceExample;
import org.binar.delapan.OptionalExample;
import org.binar.delapan.StreamExample;
import org.binar.exceptions.TryCatch;
import org.binar.service.ILambdaExample;

public class chapter3 {

  public static void main(String[] args) {
    // compilation error karena ga pake simbol titik koma (;)
//    System.out.println()
    // runtime error, di compile sukses tapi bikin error
//    System.out.println(1/0);
//    Throwable
//    infiniteRecursion();
//    TryCatch tryCatch = new TryCatch();
//    tryCatch.tryCatch(null);

//    try {
//      tryCatch.throwsExample(null);
//    } catch (IOException e) {
//      throw new RuntimeException(e);
//    }
//    tryCatch.throwNewExample("2");

//    OptionalExample optionalExample = new OptionalExample();
//    optionalExample.withOptional();
//    optionalExample.moduleExampleWOOptional();
//    optionalExample.moduleExampleWithOptional();
//
//    ILambdaExample iLambdaExample = new ILambdaExample() {
//      @Override
//      public void example(String a, Integer b) {
//        System.out.println("example of normal instantiation of interface class");
//      }
//    };
//    iLambdaExample.example("a", 10);
//
//    ILambdaExample iLambdaExample1 = (a, b) -> System.out.println("example of interface instantiation with lambda");
//    iLambdaExample1.example("a", 10);
//
//    LambdaExample lambdaExample = new LambdaExample();
//    lambdaExample.functional();

//    MethodReferenceExample methodReferenceExample = new MethodReferenceExample();
//    methodReferenceExample.methodRefInstance("test1");
//    Consumer<String> cons1 = MethodReferenceExample::methodRefStatic; // (param) -> MethodReferenceExample.methodRefStatic(param);
//
//    cons1.accept("paraaaam");
//
//    MethodReferenceExample.methodRefStatic("test2");
//    BiConsumer<MethodReferenceExample, String> bc = MethodReferenceExample::methodRefInstance; // (objclass, param) -> objclass.methodRefInstance(param);
//    bc.accept(new MethodReferenceExample(), "paraaaam");
//    MethodReferenceExample mre = new MethodReferenceExample("param");
//    Consumer<String> mre1 = MethodReferenceExample::new; // (param) -> new MethodReferenceExample(param);

//    List<String> perabot = Arrays.asList("Kursi", "Meja", "Lemari");
//    for(String per : perabot) {
//      System.out.println(per);
//    }
//
//    Consumer<String> cons = c -> perabot.forEach((per) -> {
//      System.out.println(per);
//    });
//
//    cons.accept("text");
//
//    perabot.removeIf(per -> per.length() > 2);
//    cons.accept("text");

    StreamExample streamExample = new StreamExample();
    streamExample.operation();
    streamExample.operationObj();
    streamExample.processUSB();
  }

  public static Integer infiniteRecursion() {
    return infiniteRecursion();
  }

}
