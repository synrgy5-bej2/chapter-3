package org.binar.challenge3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.stream.Collectors;
import org.binar.exceptions.RizkyExceptions;

public class FileHandlingImpl implements FileHandling{

  List<String> dataNilai;

  File file;

  public FileHandlingImpl() {}

  public FileHandlingImpl(List<String> dataNilai, File file) {
    this.file = file;
    this.dataNilai = dataNilai;
  }

  @Override
  public List<String> readFile(String path) throws IOException {
    try{
      File file = new File(path); // ngambil detail file yg kita masukan lokasi file nya
      FileReader reader = new FileReader(file);
      BufferedReader br = new BufferedReader(reader);
      String line = "";
      String[] tempArr;
//      Map<String, DataDiri> mapDd = new HashMap<>();
      while((line = br.readLine()) != null) {
        tempArr = line.split(",");
        for(int i = 0; i < tempArr.length; i++) {
          dataNilai.add(tempArr[i]);
        }
      }
      String line1 = "1234,Rizky,2010-15-15,Sunnyvale CA,0817276x";
      String[] line1Arr = {"1234", "Rizky", "2010-15-15", "Sunnivale CA", "0817276x"};
      br.close();

    } catch(IOException ioe) {
      ioe.printStackTrace();
      throw new IOException();
    }
    return dataNilai;
  }

  @Override
  public void writeFile(String path) throws IOException {
    try {
      File file = new File(path);
      file.exists();
      if(file.createNewFile()) {
        System.out.println("New file has been created!");
      } else {
        System.out.println("Rewriting file in " + path);
      }
      FileWriter writer = new FileWriter(file);
      BufferedWriter bw = new BufferedWriter(writer);
      bw.write("dataDiri");
      bw.newLine();
      bw.newLine();
      bw.write("4444");
      bw.flush();
      bw.close();
    } catch (IOException e) {
      throw new IOException("File not found");
    }

  }

  public double hitungMean(List<Integer> data) {
    double mean = 0;
    try {
      if(data.isEmpty() && data == null) throw new IOException("Data nilai kosong");
      mean = data.stream().collect(Collectors.averagingDouble(Integer::intValue));
    } catch(IOException ioe) {
      System.out.println("Error ketika memproses nilai : " + ioe.getMessage());

//      throw new InputMismatchException();
      return 0;
    }
    return mean;
  }
}
