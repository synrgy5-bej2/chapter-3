package org.binar.challenge3;

import java.io.IOException;
import java.util.List;

public interface FileHandling {

  void writeFile(String path) throws IOException;
  List<String> readFile(String path) throws IOException;

}
