package org.binar.challenge3;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Menu {

  Scanner scan;

  public Menu() {

  }

  public Menu(Scanner scan) {
    this.scan = scan;
  }

  public void mainMenu(Scanner scan) {
    try {
      System.out.println("Pilih gan\n1.freq\n2.Calc\n3.Dual");
      int opt = scan.nextInt();
      switch(opt) {
        case 1:
          menuFreq();
          break;
        case 2:
          menuCalc();
          break;
        case 3:
          menuDual();
          break;
        default:
          System.out.println("Mantap jiwa");
          break;
      }
    } catch(InputMismatchException ime) {
      System.out.println("Error! " + ime.getMessage());
    }
  }

  public void menuFreq() {

  }

  public void menuCalc() {

  }

  public void menuDual() {

  }

}
